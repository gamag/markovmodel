package gamag.markov;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Random;
import java.util.Map.Entry;

public final class MarkovBuilder {
    private final HashMap<String, HashMap<String, Double>> table = new HashMap();

    public void add(String first, String second, double n) {
        HashMap t;
        if (!this.table.containsKey(first)) {
            t = new HashMap();
            this.table.put(first, t);
        } else {
            t = (HashMap)this.table.get(first);
        }

        if (t.containsKey(second)) {
            t.put(second, (Double)t.get(second) + n);
        } else {
            t.put(second, n);
        }
    }

    public void add(String first, String second) {
        this.add(first, second, 1.0D);
    }

    public MarkovChain<String> makeChain() {
        return new Chain(this.table, null);
    }

    public MarkovChain<String> makeChain(String initial) {
        return new Chain(this.table, initial);
    }

    private static class Chain implements MarkovChain<String> {
        Random rnd;
        String current;
        HashMap<String, HashMap<String, Double>> probTable;

        public Chain(HashMap tab, String initial) {
            this.rnd = new Random();
            this.genTable(tab);
            if (initial == null) {
                this.current = (String)this.probTable.keySet().toArray()[this.rnd.nextInt(this.probTable.size())];
            } else {
                this.current = initial;
            }

        }

        public boolean isTerminal() {
            double sum = 0.0;
            double v;
            if (this.probTable.containsKey(this.current)) {
                for (Iterator var5 = ((HashMap)this.probTable.get(this.current)).values().iterator(); var5.hasNext(); sum += v) {
                    v = (Double)var5.next();
                }
            }

            return sum == 0.0D;
        }

        public String step() {
            this.choose();
            return this.current;
        }

        public String[] step(int n) {
            String[] results = new String[n];

            for(int i = 0; i < n; ++i) {
                this.choose();
                results[i] = this.current;
                if (this.isTerminal()) {
                    String[] tmp = results;
                    results = new String[i + 1];
                    System.arraycopy(tmp, 0, results, 0, i + 1);
                    break;
                }
            }

            return results;
        }

        public String getCurrent() {
            return this.current;
        }

        public String reset() {
            this.current = (String)this.probTable.keySet().toArray()[this.rnd.nextInt(this.probTable.size())];
            return this.current;
        }

        public String reset(String initial) {
            this.current = initial;
            return this.current;
        }

        private void choose() {
            double x = this.rnd.nextDouble();
            if (!this.isTerminal()) {
                Iterator var4 = ((HashMap)this.probTable.get(this.current)).entrySet().iterator();

                while(var4.hasNext()) {
                    Entry<String, Double> kv = (Entry)var4.next();
                    x -= (Double)kv.getValue();
                    if (x <= 0.0D) {
                        this.current = (String)kv.getKey();
                        break;
                    }
                }
            }

        }

        private void genTable(HashMap<String, HashMap<String, Double>> tab) {
            this.probTable = new HashMap();
            double sum = 0.0D;

            for(Iterator var5 = tab.keySet().iterator(); var5.hasNext(); sum = 0.0D) {
                String k = (String)var5.next();
                HashMap<String, Double> pt = new HashMap();
                this.probTable.put(k, pt);
                HashMap<String, Double> t = (HashMap)tab.get(k);

                double v;
                for(Iterator var10 = t.values().iterator(); var10.hasNext(); sum += v) {
                    v = (Double)var10.next();
                }

                Iterator var9 = t.keySet().iterator();

                while(var9.hasNext()) {
                    String s = (String)var9.next();
                    pt.put(s, sum == 0.0D ? 0.0D : (Double)t.get(s) / sum);
                }
            }

        }
    }
}
