package gamag.markov;

public interface MarkovChain<T> {
   boolean isTerminal();

   T step();

   T[] step(int var1);

   T getCurrent();

   T reset();

   T reset(T var1);
}
