package gamag;

import gamag.markov.MarkovBuilder;
import gamag.markov.MarkovChain;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;
import javax.swing.JTextArea;
import javax.swing.SpinnerNumberModel;
import javax.swing.SwingUtilities;

public class MainClass {
   private static JFrame frame;
   private static JTextArea input;
   private static JButton button;
   private static JSpinner spinner;

    private static String populate(MarkovBuilder mb) {
        String[] content = input.getText().split("\n");
        boolean hasContent = false;
        String[] var6 = content;
        int var5 = content.length;

        for(int var4 = 0; var4 < var5; ++var4) {
            String line = var6[var4];
            line = line.trim();
            if (!line.equals("")) {
                hasContent = true;
                String[] terms = line.split("\\s+");
            if (terms.length != 3) {
               return "LINHA:  " + line;
            }

            double n;
            try {
               n = Double.parseDouble(terms[2]);
            } catch (NumberFormatException var11) {
               return "LINHA:  " + line;
            }

            mb.add(terms[0], terms[1], n);
         }
      }

      return hasContent ? "" : "Sem dados";
   }

   static void setupGUI() {
      frame = new JFrame();
      frame.setSize(300, 400);
      frame.setTitle("Markov");
      frame.setDefaultCloseOperation(3);
      JPanel mainPanel = new JPanel();
      mainPanel.setLayout(new BorderLayout());
      JPanel controlPanel = new JPanel();
      mainPanel.add(controlPanel, "North");
      button = new JButton("Gerar");
      button.addActionListener(MainClass::actionPerformed);
      controlPanel.add(button);
      spinner = new JSpinner(new SpinnerNumberModel(10, 2, 10000, 1));
      controlPanel.add(spinner);
      input = new JTextArea(15, 25);
      JScrollPane scrollV = new JScrollPane(input);
      mainPanel.add(scrollV, "Center");
      frame.add(mainPanel);
      frame.setVisible(true);
   }

    static void actionPerformed(ActionEvent e) {
        MarkovBuilder mb = new MarkovBuilder();
        String result = populate(mb);
        if (result == "") {
            MarkovChain<String> mc = mb.makeChain();
            String initial = (String)mc.getCurrent();
            String[] generated = (String[])mc.step((Integer)MainClass.spinner.getValue() - 1);
            new ResultDialog(MainClass.frame, initial, generated);
        } else {
            JOptionPane.showMessageDialog(MainClass.frame, result, "ERRO", 0);
        }

    }

   public static void main(String[] args) {
       SwingUtilities.invokeLater(MainClass::setupGUI);
   }
}
