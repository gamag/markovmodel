package gamag;

import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

public class ResultDialog extends JDialog {
   JTextArea text = new JTextArea(20, 25);
   JScrollPane scroll;
   private static final long serialVersionUID = 1L;

   public ResultDialog(JFrame frame, String initial, String[] toShow) {
      super(frame, "Resultado", true);
      this.text.append(initial);
      this.text.append("\n");
      String[] var7 = toShow;
      int var6 = toShow.length;

      for(int var5 = 0; var5 < var6; ++var5) {
         String s = var7[var5];
         this.text.append(s);
         this.text.append("\n");
      }

      this.text.setEditable(false);
      this.text.setCaretPosition(0);
      this.scroll = new JScrollPane(this.text);
      this.add(this.scroll);
      this.pack();
      this.setDefaultCloseOperation(2);
      this.setVisible(true);
   }
}
